gogen:
	rm -rf server/mascot_registry_pb;
	mkdir server/mascot_registry_pb;
	protoc -I server/proto service.proto --go_out=plugins=grpc:"server/mascot_registry_pb"

grpc_test:
	grpcurl --plaintext localhost:3752 mascot_registry_pb.Registry/CreateDevice
	grpcurl --plaintext localhost:3752 mascot_registry_pb.Registry/DeleteDevice
	grpcurl --plaintext localhost:3752 mascot_registry_pb.Registry/ListServers
	grpcurl --plaintext localhost:3752 mascot_registry_pb.Registry/ListDevices
	grpcurl --plaintext -d '{"family": "f", "domain": "d", "identifier": "i"}' localhost:3752 mascot_registry_pb.Registry/Locate
	grpcurl --plaintext localhost:3752 mascot_registry_pb.Registry/PublishDevice
	grpcurl --plaintext localhost:3752 mascot_registry_pb.Registry/UnpublishDevice
	grpcurl --plaintext localhost:3752 mascot_registry_pb.Registry/PublishServer
	grpcurl --plaintext localhost:3752 mascot_registry_pb.Registry/UnpublishServer
