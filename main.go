package main

import (
	"flag"
	"fmt"
	"log"
	"net"

	"gitlab.maxiv.lu.se/kits-maxiv/svc-maxiv-mascot-registry/registry"
	"gitlab.maxiv.lu.se/kits-maxiv/svc-maxiv-mascot-registry/server"
	"gitlab.maxiv.lu.se/kits-maxiv/svc-maxiv-mascot-registry/server/mascot_registry_pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	var port int
	flag.IntVar(&port, "port", 3752, "The gRPC port")
	flag.Parse()

	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		log.Fatal(err)
	}

	grpcServer := grpc.NewServer()

	s := server.NewServer(registry.New())

	mascot_registry_pb.RegisterRegistryServer(grpcServer, &s)
	reflection.Register(grpcServer)

	log.Printf("Starting gRPC server on :%d\n", port)
	err = grpcServer.Serve(listener)
	if err != nil {
		log.Fatal(err)
	}
}
