package mascot_types

type MascotServer struct {
	Name string
	Host string
	Port int
}

func (m MascotServer) Published() bool {
	return m.Host != "" && m.Port != 0
}

type MascotDevice struct {
	Class      string
	Family     string
	Domain     string
	Identifier string
}
