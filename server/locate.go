package server

import (
	"context"
	"log"

	"gitlab.maxiv.lu.se/kits-maxiv/svc-maxiv-mascot-registry/server/mascot_registry_pb"
)

func (s server) Locate(_ context.Context, msg *mascot_registry_pb.DeviceName) (*mascot_registry_pb.LocateReply, error) {
	name := deviceName(msg)
	log.Printf("gRPC - Locate (%s)", name)
	v := &mascot_registry_pb.LocateReply{
		ServerName: "SomeServer",
		Host:       "localhost",
		Port:       58128,
	}

	s.registry.Locate(name)

	return v, nil
}
