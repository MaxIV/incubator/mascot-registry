package server

import (
	"context"
	"log"

	"gitlab.maxiv.lu.se/kits-maxiv/svc-maxiv-mascot-registry/server/mascot_registry_pb"
	"gitlab.maxiv.lu.se/kits-maxiv/svc-maxiv-mascot-registry/mascot_types"
)

func (s server) PublishServer(_ context.Context, msg *mascot_registry_pb.PublishServerMessage) (*mascot_registry_pb.ErrorReply, error) {
	log.Println("gRPC - PublishServer")

	server := mascot_types.MascotServer{
		Name: msg.ServerName,
		Host: msg.Host,
		Port: int(msg.Port),
	}

	s.registry.PublishServer(server)

	return &mascot_registry_pb.ErrorReply{Success: true}, nil
}

func (s server) UnpublishServer(_ context.Context, msg *mascot_registry_pb.UnpublishServerMessage) (*mascot_registry_pb.ErrorReply, error) {
	log.Println("gRPC - UnpublishServer")

	s.registry.UnpublishServer(msg.ServerName)

	return &mascot_registry_pb.ErrorReply{Success: true}, nil
}
