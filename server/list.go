package server

import (
	"context"
	"log"

	"gitlab.maxiv.lu.se/kits-maxiv/svc-maxiv-mascot-registry/server/mascot_registry_pb"
)

func (s server) ListServers(context.Context, *mascot_registry_pb.Empty) (*mascot_registry_pb.ListServersReply, error) {
	log.Println("gRPC - ListServers")
	v := &mascot_registry_pb.ListServersReply{
		ServerName: []string{
			"SomeServer",
		},
	}

	s.registry.ListServers()

	return v, nil
}

func (s server) ListDevices(_ context.Context, msg *mascot_registry_pb.ListDevicesMessage) (*mascot_registry_pb.ListDevicesReply, error) {
	log.Println("gRPC - ListDevices")
	v := &mascot_registry_pb.ListDevicesReply{
		Devices: []*mascot_registry_pb.Device{
			&mascot_registry_pb.Device{
				Mascotclass: "class",
				Name: &mascot_registry_pb.DeviceName{
					Family:     "family",
					Domain:     "domain",
					Identifier: "identifier",
				},
			},
		},
	}

	s.registry.ListDevices(msg.ServerName)

	return v, nil
}
