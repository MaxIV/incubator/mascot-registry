package server

import (
	"strings"

	"gitlab.maxiv.lu.se/kits-maxiv/svc-maxiv-mascot-registry/server/mascot_registry_pb"
	"gitlab.maxiv.lu.se/kits-maxiv/svc-maxiv-mascot-registry/mascot_types"
)

type server struct {
	registry MascotRegistry
}

type MascotRegistry interface {
	CreateDevice(deviceName []mascot_types.MascotDevice, serverName string) error
	DeleteDevice(deviceName []string) error
	PublishServer(mascot_types.MascotServer) error
	UnpublishServer(serverName string) error
	ListServers() (servers []string, err error)
	ListDevices(serverName string) (devices []string, err error)
	PublishDevice(deviceName string) error
	UnpublishDevice(deviceName string) error
	Locate(deviceName string) (mascot_types.MascotServer, error)
}

func NewServer(registry MascotRegistry) server {
	return server{registry: registry}
}

func deviceName(proto_msg *mascot_registry_pb.DeviceName) string {
	return strings.Join([]string{
		proto_msg.Domain,
		proto_msg.Family,
		proto_msg.Identifier,
	}, "/")
}
