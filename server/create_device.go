package server

import (
	"context"
	"log"

	"gitlab.maxiv.lu.se/kits-maxiv/svc-maxiv-mascot-registry/mascot_types"

	"gitlab.maxiv.lu.se/kits-maxiv/svc-maxiv-mascot-registry/server/mascot_registry_pb"
)

func (s server) CreateDevice(_ context.Context, msg *mascot_registry_pb.CreateDeviceMessage) (*mascot_registry_pb.ErrorReply, error) {
	log.Println("gRPC - CreateDevice")

	devices := []mascot_types.MascotDevice{}
	for _, d := range msg.Devices {
		devices = append(devices, mascot_types.MascotDevice{
			Class:      d.Mascotclass,
			Family:     d.Name.Family,
			Domain:     d.Name.Domain,
			Identifier: d.Name.Identifier,
		})
	}

	s.registry.CreateDevice(devices, msg.ServerName)

	return &mascot_registry_pb.ErrorReply{Success: true}, nil
}

func (s server) DeleteDevice(_ context.Context, msg *mascot_registry_pb.DeleteDeviceMessage) (*mascot_registry_pb.ErrorReply, error) {
	log.Println("gRPC - DeleteDevice")

	names := []string{}
	for _, d := range msg.Devices {
		names = append(names, deviceName(d))
	}

	s.registry.DeleteDevice(names)

	return &mascot_registry_pb.ErrorReply{Success: true}, nil
}
