package server

import (
	"context"
	"log"

	"gitlab.maxiv.lu.se/kits-maxiv/svc-maxiv-mascot-registry/server/mascot_registry_pb"
)

func (s server) PublishDevice(_ context.Context, msg *mascot_registry_pb.DeviceName) (*mascot_registry_pb.ErrorReply, error) {
	log.Println("gRPC - PublishDevice")

	s.registry.PublishDevice(deviceName(msg))

	return &mascot_registry_pb.ErrorReply{Success: true}, nil
}

func (s server) UnpublishDevice(_ context.Context, msg *mascot_registry_pb.DeviceName) (*mascot_registry_pb.ErrorReply, error) {
	log.Println("gRPC - UnpublishDevice")

	s.registry.UnpublishDevice(deviceName(msg))

	return &mascot_registry_pb.ErrorReply{Success: true}, nil
}
