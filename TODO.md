# Todo

* Support multiple ports for different purposes
  - For reading/writing attributes and running commands (gRPC)
  - For publishing events (ZeroMQ)
* Maybe support that the incoming IP for the PublishServer request is also used for a server (RemoteAddr)
*
