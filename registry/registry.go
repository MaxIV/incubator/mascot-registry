package registry

import (
	"log"
	"strings"

	"gitlab.maxiv.lu.se/kits-maxiv/svc-maxiv-mascot-registry/mascot_types"
)

type registry struct{}

func New() registry {
	return registry{}
}

func (r registry) CreateDevice(deviceName []mascot_types.MascotDevice, serverName string) error {
	log.Printf("registry - CreateDevice - Devices: %+v, server: %s", deviceName, serverName)
	return nil
}
func (r registry) DeleteDevice(deviceName []string) error {
	log.Printf("registry - DeleteDevice - Devices: %s", strings.Join(deviceName, ", "))
	return nil
}
func (r registry) PublishServer(server mascot_types.MascotServer) error {
	log.Printf("registry - PublishServer %+v", server)
	return nil
}
func (r registry) UnpublishServer(serverName string) error {
	log.Printf("registry - UnpublishServer %s", serverName)
	return nil
}
func (r registry) ListServers() (servers []string, err error) {
	log.Printf("registry - ListServers %s", strings.Join(servers, ", "))
	return []string{}, nil
}
func (r registry) ListDevices(serverName string) (devices []string, err error) {
	log.Printf("registry - ListDevices %s", serverName)
	return []string{}, nil
}
func (r registry) PublishDevice(deviceName string) error {
	log.Printf("registry - PublishDevice %s", deviceName)
	return nil
}
func (r registry) UnpublishDevice(deviceName string) error {
	log.Printf("registry - UnpublishDevice %s", deviceName)
	return nil
}
func (r registry) Locate(deviceName string) (mascot_types.MascotServer, error) {
	log.Printf("registry - Locate %s", deviceName)
	return mascot_types.MascotServer{}, nil
}
